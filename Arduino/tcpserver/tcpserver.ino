#include <ESP8266WiFi.h>

WiFiServer server(23);

void setup()
{
    Serial.begin(115200);
    WiFi.mode(WIFI_STA);
    WiFi.begin("LIUYU", "12345678");
    while (!WiFi.isConnected())
    {
        delay(500);
        Serial.print('.');
    }
    Serial.println(WiFi.localIP());
    pinMode(D1, OUTPUT);
    digitalWrite(D1, HIGH); // Relay off
    server.begin();
}

void loop()
{
    WiFiClient client = server.available();
    if (!client)
    {
        return;
    }

    Serial.print(client.remoteIP());
    Serial.println(" connected");

    while (client.connected())
    {
        if (client.available())
        {
            int c = client.read();
            if (c == '1')
            {
                digitalWrite(D1, LOW); // Relay On
            }
            if (c == '0')
            {
                digitalWrite(D1, HIGH); // Relay Off
            }
        }
    }
}
