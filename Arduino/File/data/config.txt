#include <FS.h>

void setup() {
    Serial.begin(115200);
    SPIFFS.begin();
    File f = SPIFFS.open("config.txt", "r");
    if (!f)
    {
        Serial.println("open file failed");
        return;
    }
    Serial.println(f.readString());
    f.close();
}

void loop() {
  // put your main code here, to run repeatedly:

}