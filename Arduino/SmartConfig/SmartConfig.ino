//使用ESPTouch自动配置WiFi网络
//客户端：https://github.com/EspressifApp/EsptouchForAndroid/tree/master/releases/apk

#include <ESP8266WiFi.h>

void setup(void) {
    Serial.begin(115200);
    // 必须采用Station模式
    WiFi.mode(WIFI_STA);
    // 等待配网
    WiFi.beginSmartConfig();
    // 收到配网信息后ESP8266将自动连接
    while (!WiFi.smartConfigDone()) {
        delay(500);
    }

    Serial.println("");
    Serial.println("WiFi connected");
    Serial.println("IP address: ");
    Serial.println(WiFi.localIP());
}

void loop(void) {}
