#include "wificonfig.h"

//使用ESPTouch自动配置WiFi
void smartConfig()
{
    pinMode(LED_BUILTIN, OUTPUT);
    WiFi.beginSmartConfig();
    while (!WiFi.smartConfigDone()) {
        digitalWrite(LED_BUILTIN, LOW); //LED On
        delay(500);
        digitalWrite(LED_BUILTIN, HIGH); //LED Off
        delay(500);
    }
    digitalWrite(LED_BUILTIN, LOW); //LED On
}

//使用上一次的WiFi配置
bool lastConfig()
{
    int i = 0;
    pinMode(LED_BUILTIN, OUTPUT);
    digitalWrite(LED_BUILTIN, HIGH); //LED Off
    WiFi.begin();

    //如果5秒内没有连接到WiFi则返回
    while (!WiFi.isConnected() && i++ < 5)
    {
        delay(1000);
    }
    if (WiFi.isConnected())
    {
        digitalWrite(LED_BUILTIN, LOW); //LED On
        return true;
    }
    return false;
}

void autoConfig(void)
{
    WiFi.mode(WIFI_STA);
    if (!lastConfig())
    {
        Serial.println("Run SmartConfig");
        smartConfig();
        delay(2000);//等待WiFi连接建立
    }
}
