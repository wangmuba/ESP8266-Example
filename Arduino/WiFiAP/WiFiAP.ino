#include <ESP8266WiFi.h>

void setup() {
    Serial.begin(115200);
    WiFi.mode(WIFI_AP);
    String ssid = "ESP_" + String(ESP.getChipId(), HEX);
    ssid.toUpperCase();
    WiFi.softAP(ssid, "12345678");
    Serial.println(WiFi.softAPIP().toString());
}

void loop() {
  // put your main code here, to run repeatedly:
}
